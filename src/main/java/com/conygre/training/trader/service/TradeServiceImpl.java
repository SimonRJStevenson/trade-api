package com.conygre.training.trader.service;

import java.util.Collection;

import com.conygre.training.trader.dao.TradeRepo;
import com.conygre.training.trader.model.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepo repo;

    @Override
    public void addTrade(Trade trade) {
        repo.insert(trade);
    }

    @Override
    public Collection<Trade> getAllTrades() {
        return repo.findAll();
    }
}